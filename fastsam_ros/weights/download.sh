#!/bin/bash

cd $(dirname $0);

wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=10XmSj6mmpmRb8NhXbtiuO9cTTBwR_9SV' -O FastSAM-s.pt
wget https://huggingface.co/spaces/An-619/FastSAM/resolve/main/weights/FastSAM.pt
