#! /usr/bin/env python3
#coding:utf-8

import ast
import time
from threading import Lock
from pathlib import Path
import torch 
import numpy as np
from PIL import Image as PILImage
import clip

import actionlib
import rospy 
import cv2
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import Header
from fastsam_ros_msgs.msg import Detection, DetectionsArray
from fastsam_ros_msgs.msg import CheckForSegmentationsAction, CheckForSegmentationsResult, CheckForSegmentationsFeedback

from submodules.FastSAM.fastsam import FastSAM, FastSAMPrompt
from submodules.FastSAM.utils.tools import convert_box_xywh_to_xyxy

current_directory = Path(__file__).parent

class FastSAMNode:
    
    def __init__(self):
        # Parameter
        self._segmentation_mode = rospy.get_param("~segmentation_mode", "4")
        self._iou_threshold = rospy.get_param("~iou_threshold", 0.9)
        self._conf_threshold = rospy.get_param("~conf_threshold", 0.4)
        text_prompt_str = rospy.get_param("~text_prompt", "None")
        self._text_prompt = None if text_prompt_str == "None" else text_prompt_str
        self._point_prompt = ast.literal_eval(rospy.get_param("~point_prompt", "[[0, 0]]"))
        self._point_label = ast.literal_eval(rospy.get_param("~point_label", "[1]"))
        bbox_prompt = ast.literal_eval(rospy.get_param("~box_prompt", "[[0,0,0,0]]"))
        self._box_prompt = convert_box_xywh_to_xyxy(bbox_prompt)
        self._random_color = rospy.get_param("~random_color", True)
        self._better_quality = rospy.get_param("~better_quality", False)
        self._retina = rospy.get_param("~retina", False)
        self._with_contours = rospy.get_param("~with_contours", False)
        self._imgsz = rospy.get_param("~imgsz", 640)
        self._device = rospy.get_param("~device", "cuda")
        weight_path = rospy.get_param("~weight_path", str(current_directory.parent) + "/weights/FastSAM.pt")
        self._frame_rate = rospy.get_param("~frame_rate", 10)
        self._before_time = rospy.get_param("~before_time", 0)
        self._loop_rate = rospy.get_param("~loop_rate", 60)
        
        self._model = FastSAM(weight_path)
        self._cv_bridge = CvBridge()
        self._lock = Lock()
        self._frame_time = 1.0 / self._frame_rate

        # Subscriber
        self._is_callback_registered = False
        self._subscrived_msg = None
        self._subscriber_args = ("/input/image/compressed", CompressedImage, self._callback)
        self._subscriber = None

        # Publisher
        self._segmentation_image_publisher = rospy.Publisher("/output/image/compressed", CompressedImage, queue_size=1)
        self._segmentations_publisher = rospy.Publisher("/output/segmentations", DetectionsArray, queue_size=1)

        # Action Server
        self._server = actionlib.SimpleActionServer("/check_for_segmentations", CheckForSegmentationsAction, self._action_call_back, auto_start=False)
        self._server.start()

    # ==================================================================================================
    #
    #   Main
    #
    # ==================================================================================================
    def _main(self):
        rospy.loginfo("Start FastSAM Node")
        r = rospy.Rate(self._loop_rate)
        while not rospy.is_shutdown():
            with self._lock:
                msg = self._subscrived_msg
                self._subscrived_msg = None 

                connection_exists = (self._segmentation_image_publisher.get_num_connections() > 0) or (self._segmentations_publisher.get_num_connections() > 0)
                if connection_exists and (not self._is_callback_registered):
                    self._subscriber = rospy.Subscriber(*self._subscriber_args)
                    self._is_callback_registered = True
                    rospy.loginfo(f"Register subscriber: {self._subscriber.resolved_name}")

                elif (not connection_exists) and self._is_callback_registered:
                    rospy.loginfo(f"Unregister subscriber: {self._subscriber.resolved_name}")
                    self._subscriber.unregister()
                    self._subscriber = None
                    self._is_callback_registered = False

            if msg is None:
                r.sleep()
                continue
        
    # ==================================================================================================
    #
    #   ROS Callback
    #
    # ==================================================================================================
    
    def _callback(self, msg: CompressedImage):
        # 一定の時間間隔で処理し, システムのリソースを節約
        now_time = time.time()
        if (now_time - self._before_time) < self._frame_time:
            return

        self._before_time = now_time

        with self._lock:
            self._subscrived_msg = msg
            rgb_np = self._cv_bridge.compressed_imgmsg_to_cv2(msg).copy()
            rgb_pil = PILImage.fromarray(rgb_np)

            segmentations, segmented_img = self._predict(rgb_pil)

            segmented_img_msg = self._cv_bridge.cv2_to_compressed_imgmsg(segmented_img)
            segmented_img_msg.header.stamp = rospy.Time.now()
            self._segmentation_image_publisher.publish(segmented_img_msg)
            self._segmentations_publisher.publish(segmentations)

            elapsed_time = time.time() - now_time
            rospy.loginfo(f"elapsed_time: {elapsed_time:.2f}[sec]")
    
    def _action_call_back(self, goal):
        """
        Args:
            goal (CheckForSegmentations):
        """
        rospy.loginfo("Receive ActionGoal")
        rgb_np = self._cv_bridge.compressed_imgmsg_to_cv2(goal.image).copy()
        rgb_pil = PILImage.fromarray(rgb_np)
        segmentations, segmented_img = self._predict(rgb_pil)
        segmented_img_msg = self._cv_bridge.cv2_to_compressed_imgmsg(segmented_img)

        if not self._server.is_preempt_requested():
            result = CheckForSegmentationsResult()
            result.id = goal.id
            result.segmented_image = segmented_img_msg
            result.segmentations = segmentations
            self._server.set_succeeded(result)
            rospy.loginfo("Return ActionResult")

    # ==================================================================================================
    #
    #   Instance Method (Private)
    #
    # ==================================================================================================
    
    def _predict(self, rgb_img: PILImage):
        with torch.no_grad():
            everything_result = self._model(rgb_img, 
                                            device = self._device, 
                                            retina_masks = self._retina,
                                            imgsz=self._imgsz, 
                                            conf = self._conf_threshold, 
                                            iou = self._iou_threshold)
        
        # Segmentation情報をROSのmsg化
        single_result = everything_result[0]
        prompt_segmentation = FastSAMPrompt(rgb_img, single_result)
        annotations = prompt_segmentation._format_results(single_result)

        detections_array_msg = DetectionsArray()
        for annotation in annotations:
            detection_msg = self._create_detection_msg(annotation['id'], annotation['segmentation'], annotation['bbox'], annotation['score'], annotation['area'])
            detections_array_msg.segmentations.append(detection_msg)
        
        # Segmentation画像をROSのmsg化
        boxes = None
        points = None 
        point_label = None 
        prompt_image = FastSAMPrompt(rgb_img, everything_result, device=self._device)
        
        if str(self._segmentation_mode) == "0":
            """
            Segments regions based on the provided text description using text prompt.

            Parameters:
            - text: Text description for segmentation, e.g., 'a photo of a dog'

            Returns:
            - ann: Segmented annotations.
            """
            ann = prompt_image.text_prompt(text="car toy")

        elif str(self._segmentation_mode) == "1":
            """
            Segments regions around provided points using point prompt.

            Parameters:
            - points: List of [x, y] coordinates, e.g., [[x1, y1], [x2, y2]]
            - point_label: Labels for each point (0: background, 1: foreground)

            Returns:
            - ann: Segmented annotations.
            """
            ann = prompt_image.point_prompt(
                points=[self._point_prompt], pointlabel=self._point_label
            )

            points = [self._point_prompt]
            point_label = self._point_label

        elif str(self._segmentation_mode) == "2":
            """
            Segments regions based on the provided bounding box using box prompt.

            Parameters:
            - boxes: Bounding box coordinates for segmentation, e.g., [[200, 200, 300, 300]]

            Returns:
            - ann: Segmented annotations.
            """
            ann = prompt_image.box_prompt(bbox=self._box_prompt[0])
            boxes = self._box_prompt

        else:
            """
            Segments all detectable objects in the image using the everything prompt.

            Returns:
            - ann: Segmented annotations for all detected objects.
            """
            ann = prompt_image.everything_prompt()
        
        segmented_img = prompt_image.plot_to_result(annotations=ann,
                                                    bboxes=boxes,
                                                    points=points,
                                                    point_label=point_label,
                                                    mask_random_color=self._random_color,
                                                    better_quality=self._better_quality,
                                                    retina=self._retina,
                                                    withContours=self._with_contours)
        
        del everything_result
        del single_result
        
        return detections_array_msg, segmented_img

    def _compute_center_of_segmentation_in_rgb(self, binary_mask):
        # セグメントされた領域のピクセルのインデックスを取得
        y_indices, x_indices = np.where(binary_mask == 1)  # または binary_mask == 255

        # インデックスの平均値を計算
        center_y = np.mean(y_indices)
        center_x = np.mean(x_indices)

        return center_x, center_y
    
    def _create_detection_msg(self, id, segmentation_2d, bbox_tensor, score, area):
        # セグメンテーションマスクを圧縮
        retval, buffer = cv2.imencode('.png', segmentation_2d * 255)  # 0-1のデータを0-255に変換

        # CompressedImageメッセージを作成
        compressed_image_msg = CompressedImage()
        compressed_image_msg.header.stamp = rospy.Time.now()
        compressed_image_msg.format = "png"
        compressed_image_msg.data = np.array(buffer).tobytes()

        # Detectionメッセージを作成
        detection_msg = Detection()
        detection_msg.id = id
        detection_msg.segmented_image = compressed_image_msg
        bbox = bbox_tensor[:4].tolist()
        detection_msg.bbox = bbox
        detection_msg.score = score
        detection_msg.area = area
        center_x, center_y = self._compute_center_of_segmentation_in_rgb(segmentation_2d)
        detection_msg.center = [center_x, center_y]

        return detection_msg
        
if __name__ == "__main__":
    rospy.init_node('fastsam_node')
    fastsam_node = FastSAMNode()
    fastsam_node._main()