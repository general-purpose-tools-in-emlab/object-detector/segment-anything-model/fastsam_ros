<!----------------------------------------------------------------------------------------------------------------------
#
#   Title
#
# --------------------------------------------------------------------------------------------------------------------->
# FastSAM-ROS
<!----------------------------------------------------------------------------------------------------------------------
#
#   Description
#
# --------------------------------------------------------------------------------------------------------------------->
- ROS wrapper repository for [FastSAM](https://github.com/CASIA-IVA-Lab/FastSAM/tree/main)  
- Topic and Action communication is available  

![Input Image](docs/output.gif)  


*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

<!----------------------------------------------------------------------------------------------------------------------
#
#   Table of Contents
#
# --------------------------------------------------------------------------------------------------------------------->
**Table of Contents:**
*   [Setup](#Setup)
*   [Launch](#launch)
*   [Files](#files)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Requirement
#
# --------------------------------------------------------------------------------------------------------------------->
## Requirements
* Required
    * Ubuntu: 20.04  
    * ROS: Noetic  
    * Python: 3.8

* Recommends
    * pytorch>=1.7
    * torchvision>=0.8

* Confirmed
```
Ubuntu: 20.04LTS  
ROS: Noetic  
Python: 3.8.10  
torch: 1.9.1+cu111 
torchvision: 0.10.1+cu111
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   Dependencies
#
# --------------------------------------------------------------------------------------------------------------------->
## Dependencies
Code wrapped from https://github.com/CASIA-IVA-Lab/FastSAM#installation  

- URL: https://github.com/CASIA-IVA-Lab/FastSAM#installation  
- Tag: `v0.0.2`  
- Commit: `38654b76b31d3178a8572579968f8eb404936318`  
- Original README.md: https://github.com/CASIA-IVA-Lab/FastSAM/tree/38654b76b31d3178a8572579968f8eb404936318  
- Original LICENSE: https://github.com/CASIA-IVA-Lab/FastSAM/blob/38654b76b31d3178a8572579968f8eb404936318/LICENSE  

<!----------------------------------------------------------------------------------------------------------------------
#
#   Getting Started
#
# --------------------------------------------------------------------------------------------------------------------->
## Getting Started
```shell
git clone --recursive https://gitlab.com/general-purpose-tools-in-emlab/object-detector/segment-anything-model/fastsam_ros

```

```shell
cd fastsam_ros
pip install -r requirements.txt
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   Weights
#
# --------------------------------------------------------------------------------------------------------------------->
## Weights
Get network weights by the shell. (`FastSAM-s.pt` and `FastSAM.pt`)  

```shell
roscd fastsam_ros/weights
bash download.sh
```

Please download `FastSAM-x.pt` manually.  
https://drive.google.com/file/d/1m1sjY4ihXBU1fZXdQ-Xdj-mDltW-2Rqv/view  

<!----------------------------------------------------------------------------------------------------------------------
#
#   Example
#
# --------------------------------------------------------------------------------------------------------------------->
## Example
weight: `FastSAM.pt`
```shell
roslaunch fastsam_ros fastsam_node.launch
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   ROS API
#
# --------------------------------------------------------------------------------------------------------------------->
## ROS API
### FastSAM Node

#### Subscribed Topics
* `/input/image/compressed` (sensor_msgs/CompressedImage)

#### Published Topics
* `/output/segmentations` (fastsam_ros_msgs/DetectionsArray)
* `/output/image/compressed` (sensor_msgs/CompressedImage)

#### Action Subscribed Namespace
* `/check_for_segmentations` (fastsam_ros_msgs/CheckForSegmentationsAction)

#### Parameters
* `~segmentation_mode` (str, default="4")
* `~iou_threshold` (double, default=0.9)
* `~conf_threshold` (double, default=0.4)
* `~text_prompt` (str, default="None")
* `~point_prompt` (str, default="[[0, 0]]")
* `~point_label` (str, default="[0]")
* `~box_prompt` (str, default="[[0,0,0,0]]")
* `~random_color` (bool, default=True)
* `~better_quality` (bool, default=False)
* `~retina` (bool, default=False)
* `~with_contours` (bool, default=False)
* `~imgsz` (int, default=640)
* `~device` (str, default="cuda")
* `~weight_path` (str, default=str(current_directory.parent) + "/weights/FastSAM.pt")
* `~frame_rate` (int, default=10)
* `~before_time` (int, default=0)
* `~loop_rate` (int, default=60)

#### Segmenatation Mode
* "0": `text prompt` (e.g., 'a photo of a dog')

* "1": `point prompt`
    - points: List of [x, y] coordinates, e.g., [[x1, y1], [x2, y2]]
    - point_label: Labels for each point (0: background, 1: foreground)

* "2": `box prompt`
    - boxes: Bounding box coordinates for segmentation, e.g., [[200, 200, 300, 300]]

* "3": `everything prompt` (parameters are nothing. Segments all detectable objects in the image. )

<!----------------------------------------------------------------------------------------------------------------------
#
#   License
#
# --------------------------------------------------------------------------------------------------------------------->
## License
GPL-3.0 License (see [LICENSE](LICENSE)).  
Original LICENSE: https://github.com/CASIA-IVA-Lab/FastSAM/blob/38654b76b31d3178a8572579968f8eb404936318/LICENSE

<!----------------------------------------------------------------------------------------------------------------------
#
#   References
#
# --------------------------------------------------------------------------------------------------------------------->
## References
[FastSAM](https://github.com/CASIA-IVA-Lab/FastSAM/tree/main)